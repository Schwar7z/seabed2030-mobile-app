package com.sb2030.loggerapp.messaging.log;

import com.sb2030.loggerapp.data.types.LogMessage;

import java.util.ArrayList;

/**
 * LogSubscriber
 * This represents classes that are able to display the log items as they are generated
 * Currently this is only LogActivity. This is also why I have my own log class implemented.
 */
public interface LogSubscriber {
    public void newUpdatesAvailable( ArrayList< LogMessage > messages );
}
