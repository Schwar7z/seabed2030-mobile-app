package com.sb2030.loggerapp.messaging.ship;

import com.sb2030.loggerapp.data.types.Ship;
/**
 * ShipSubscriber
 * Implement this in a class to have that class respond to whichever ship is being
 * selected
 */
public interface ShipSubscriber {
	void updateShip( Ship s );
}
