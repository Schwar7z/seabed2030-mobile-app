package com.sb2030.loggerapp.messaging.btstatus;

import java.util.Vector;

public class BluetoothStatusMessenger {
	private static Vector< BluetoothStatusSubscriber> list = new Vector<>();
	/**
	 * Subscribe
	 * Allows a class to put itself on the "mailing list" for toast notifications
	 *
	 * @param t The class extending ToastSubscriber
	 */
	public static void subscribe( BluetoothStatusSubscriber t ) {
		list.add( t );
	}

	/**
	 * Unsubscribe
	 * Removes the class from the list. Usually called on onPause.
	 * @param t The class.
	 */
	public static void unsubscribe( BluetoothStatusSubscriber t ) {
		list.remove( t );
	}

	/**
	 * Send
	 * Sends a message to each class that has subscribed to this list
	 *
	 * @param msg The message to be passed
	 */
	public static void send( String msg ) {
		for ( BluetoothStatusSubscriber sub: list ) {
			sub.updateBluetoothStatus( msg );
		}
	}
}
