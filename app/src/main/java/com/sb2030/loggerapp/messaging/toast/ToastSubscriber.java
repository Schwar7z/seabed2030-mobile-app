package com.sb2030.loggerapp.messaging.toast;

/**
 * ToastSubscriber
 * Implement this in a class to have that class listen for Toast events.
 * Usually this will be in a class that has a UI .xml associated with it.
 * EG: ( MainViewActivity.java and activity_ship_view.xml )
 */
public interface ToastSubscriber {
	void displayToast( String msg );
}
