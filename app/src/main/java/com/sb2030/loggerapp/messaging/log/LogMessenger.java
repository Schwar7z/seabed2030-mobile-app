package com.sb2030.loggerapp.messaging.log;

import com.sb2030.loggerapp.data.types.LogMessage;

import java.util.ArrayList;
import java.util.Vector;

public class LogMessenger {
    private static Vector< LogSubscriber> list = new Vector<>();
    /**
     * Subscribe
     * Allows a class to put itself on the "mailing list" for the selected intent
     *
     * @param t The class extending IntentSubscriber
     */
    public static void subscribe( LogSubscriber t ) {
        if ( !list.contains( t ) ) {
            list.add( t );
        }
    }

    /**
     * Unsubscribe
     * Removes the class from the list. Usually called on onPause.
     * @param t The class.
     */
    public static void unsubscribe( LogSubscriber t ) {
        list.remove( t );
    }

    /**
     * Send
     * Sends the intent
     * @param messages The list of messages to tack on.
     */
    public static void update( ArrayList< LogMessage > messages ) {
        for ( LogSubscriber sub: list ) {
            sub.newUpdatesAvailable( messages );
        }
    }
}
