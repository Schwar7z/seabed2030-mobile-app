package com.sb2030.loggerapp.messaging.btgatt;

public interface BluetoothGATTSubscriber {
    void updateGattInformation( String msg );
}
