package com.sb2030.loggerapp.messaging.toast;

import java.util.Vector;

/**
 * ToastMessenger
 * Handles the toast messages being passed to activities
 */
public class ToastMessenger {
	private static Vector< ToastSubscriber > list = new Vector<>();

	/**
	 * Subscribe
	 * Allows a class to put itself on the "mailing list" for toast notifications
	 *
	 * @param t The class extending ToastSubscriber
	 */
	public static void subscribe( ToastSubscriber t ) {
		list.add( t );
	}

	/**
	 * Unsubscribe
	 * Removes the class from the list. Usually called on onPause.
	 * @param t The class.
	 */
	public static void unsubscribe( ToastSubscriber t ) {
		list.remove( t );
	}

	/**
	 * Send
	 * Sends a message to each class that has subscribed to this list
	 *
	 * @param msg The message to be passed
	 */
	public static void send( String msg ) {
		for ( ToastSubscriber sub: list ) {
			sub.displayToast( msg );
		}
	}
}
