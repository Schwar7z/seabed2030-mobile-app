package com.sb2030.loggerapp.messaging.btgatt;

import java.util.Vector;

public class BluetoothGATTMessenger {

    private static Vector< BluetoothGATTSubscriber > list = new Vector<>();
    /**
     * Subscribe
     * Allows a class to put itself on the "mailing list" for the Bluetooth GATT info
     *
     * @param t The class extending IntentSubscriber
     */
    public static void subscribe( BluetoothGATTSubscriber t ) {
        if ( !list.contains( t ) ) {
            list.add( t );
        }
    }

    /**
     * Unsubscribe
     * Removes the class from the list. Usually called on onPause.
     * @param t The class.
     */
    public static void unsubscribe( BluetoothGATTSubscriber t ) {
        list.remove( t );
    }

    /**
     * Send
     * Sends the GATT message
     *
     * @param msg The message to send
     */
    public static void send( String msg ) {
        System.out.println( "There are " + list.size() + " subscribers" );
        for ( BluetoothGATTSubscriber sub: list ) {
            sub.updateGattInformation( "[ GATT ] " + msg );
        }
    }
}
