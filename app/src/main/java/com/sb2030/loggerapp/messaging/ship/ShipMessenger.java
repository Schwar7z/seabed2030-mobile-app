package com.sb2030.loggerapp.messaging.ship;

import com.sb2030.loggerapp.data.types.Ship;

import java.util.Vector;

public class ShipMessenger {
	
	private static Vector< ShipSubscriber> list = new Vector<>();
	/**
	 * Subscribe
	 * Allows a class to put itself on the "mailing list" for the selected ship
	 *
	 * @param t The class extending ShipSubscriber
	 */
	public static void subscribe( ShipSubscriber t ) {
		if ( !list.contains( t ) ) {
			list.add( t );
		}
	}

	/**
	 * Unsubscribe
	 * Removes the class from the list. Usually called on onPause.
	 * @param t The class.
	 */
	public static void unsubscribe( ShipSubscriber t ) {
		list.remove( t );
	}

	/**
	 * Send
	 * Sends the ship that is being selected to the class
	 *
	 * @param s The ship to be passed
	 */
	public static void send( Ship s ) {
		for ( ShipSubscriber sub: list ) {
			sub.updateShip( s );
		}
	}
}
