package com.sb2030.loggerapp.messaging.btstatus;

public interface BluetoothStatusSubscriber {
	void updateBluetoothStatus( String msg );
}
