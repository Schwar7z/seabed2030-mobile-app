package com.sb2030.loggerapp.ui.fragments;


import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.sb2030.loggerapp.R;
import com.sb2030.loggerapp.data.data_handlers.ShipHandler;
import com.sb2030.loggerapp.data.types.LogMessage;
import com.sb2030.loggerapp.data.types.Ship;
import com.sb2030.loggerapp.messaging.btgatt.BluetoothGATTMessenger;
import com.sb2030.loggerapp.messaging.btgatt.BluetoothGATTSubscriber;
import com.sb2030.loggerapp.messaging.ship.ShipMessenger;
import com.sb2030.loggerapp.messaging.ship.ShipSubscriber;
import com.sb2030.loggerapp.resources.Log;
import com.sb2030.loggerapp.resources.Settings;

/**
 * A simple {@link Fragment} subclass.
 */
public class ShipDetailFrag extends Fragment implements ShipSubscriber, BluetoothGATTSubscriber {

    /**
     * Empty constructor that's required for some reason
     */
    public ShipDetailFrag() { }

    @Override
    public View onCreateView( @NonNull LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState ) {

        ShipMessenger.subscribe( this );
        BluetoothGATTMessenger.subscribe( this );

        // Inflate the layout for this fragment
        return inflater.inflate( R.layout.fragment_ship_detail, container, false );
    }

    @Override
    public void onPause() {
        super.onPause();
        ShipMessenger.unsubscribe( this );
        BluetoothGATTMessenger.unsubscribe( this );
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void updateShip( Ship s ) {
        // For now.
        if ( getContext() == null || getActivity() == null )
            return;

        Button btnRefresh = getActivity().findViewById( R.id.button_connect );
        Button btnUpload = getActivity().findViewById( R.id.button_send_data );

        if ( Settings.IS_BLUETOOTH_CONNECTED )
            btnRefresh.setText( R.string.button_connect_text_disconnect );

        // Set the buttons to be enabled, as there is now a ship lined up for them.
        btnRefresh.setEnabled( true );
        btnUpload.setEnabled( true );

        // Set the top label to the name of this ship
        Resources res = getResources();

        LinearLayout sv = getActivity().findViewById( R.id.ship_info_container );
        sv.removeAllViews();

        TextView shipHeader = getActivity().findViewById( R.id.ship_name_label );
        shipHeader.setText( res.getString( R.string.ship_detail_name, s.getName() ) );

        // Add labels based off that ship's information
        TextView shipIdInfo = new TextView( getContext() );
        shipIdInfo.setText( res.getString( R.string.ship_detail_ID, s.getId() ) );

        TextView shipLastCheckInInfo = new TextView( getContext() );
        shipLastCheckInInfo.setText( res.getString(
                R.string.ship_detail_last_check_in, s.getLastCheckInTime() ) );

        TextView shipData = new TextView( getContext() );
        shipData.setText( res.getString(
                R.string.ship_detail_data_preview,
                s.getData() ) );

        TextView btGatt = new TextView( getContext() );
        btGatt.setId( R.id.dynamic_text_gatt_messages );

        TextView btGattLabel = new TextView( getContext() );
        btGattLabel.setText( getResources().getText( R.string.bt_gatt_header_text ) );


        // Create the layout parameters, and give it to the button. Basically, just use the
        // defaults.
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );

        lp.bottomMargin = 5;

        LinearLayout.LayoutParams lp_mach_parent = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
        );

        lp_mach_parent.bottomMargin = 5;

        sv.addView( shipIdInfo, lp );
        sv.addView( shipLastCheckInInfo, lp );
        sv.addView( shipData, lp );
        sv.addView( btGattLabel, lp );
        sv.addView( btGatt, lp_mach_parent );
    }

    /**
     * UpdateGattInformation
     * Updates a text box with what's happening with the GATT connection.
     *
     * @param msg The message to send.
     */
    @Override
    public void updateGattInformation( String msg ) {

        // To eliminate the warnings
        if ( getActivity() == null )
            return;

        // To eliminate the warnings
        if ( getActivity() == null )
            return;

        // We need to create a runnable that has the argument of msg.
        class OneOff implements Runnable {
            private String str;
            private TextView btGatt;
            private TextView uploadButton;

            private OneOff( String s ) {
                this.str = s;
                if ( getActivity() != null ) {
                    btGatt = getActivity().findViewById( R.id.dynamic_text_gatt_messages );
                    uploadButton = getActivity().findViewById( R.id.button_connect );
                } else
                    Log.log( "OneOff", LogMessage.MESSAGE_TYPE.ERROR, "Could not getActivity()" );
            }

            public void run() {
                // If the fragment is being told that it's disconnected, update the
                if ( str.toLowerCase().contains( "disconnected." ) ) {
                    uploadButton.setText( R.string.button_connect_text_connect );

                    ShipHandler.getInstance().getCurrentShip().updateGattData( str );

                    // btGatt will be null if the user chooses to hide the GATT output.
                    if ( btGatt != null ){
                        btGatt.append( "[ GATT ] Disconnected." );
                        btGatt.append( "\n" );
                    }
                } else if ( str.toLowerCase().contains( "connected." ) )
                    uploadButton.setText( R.string.button_connect_text_disconnect );
                else {
                    ShipHandler.getInstance().getCurrentShip().updateGattData( str );
                    if ( btGatt != null ) {
                        btGatt.append( str );
                        btGatt.append( "\n" );
                    }
                }
            }
        }
        getActivity().runOnUiThread( new OneOff( msg ) );

    }
}
