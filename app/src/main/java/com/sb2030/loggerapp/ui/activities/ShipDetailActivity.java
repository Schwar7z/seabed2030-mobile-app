package com.sb2030.loggerapp.ui.activities;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.TextView;

import com.sb2030.loggerapp.R;
import com.sb2030.loggerapp.data.data_handlers.AWSHandler;
import com.sb2030.loggerapp.data.data_handlers.BTHandler;
import com.sb2030.loggerapp.data.data_handlers.ShipHandler;
import com.sb2030.loggerapp.messaging.ship.ShipMessenger;
import com.sb2030.loggerapp.resources.Settings;

public class ShipDetailActivity extends AppCompatActivity {

	private static ShipDetailActivity shipDetailContext;

	@Override
	protected void onCreate( Bundle savedInstanceState ) {
		super.onCreate( savedInstanceState );
		setContentView( R.layout.activity_ship_detail );

		shipDetailContext = this;

		Toolbar toolbar = findViewById( R.id.toolbar );
		String titleText = getResources().getString(
				R.string.title_activity_ship_detail,
				ShipHandler.getInstance().getCurrentShip().getName() );

		toolbar.setTitle( titleText );

		setSupportActionBar( toolbar );

		getSupportActionBar().setDisplayHomeAsUpEnabled( true );

		TextView t = findViewById( R.id.ship_name_label );
		t.setText( "" );

		// Use the ship messenger system to update the ship info.
		ShipMessenger.send( ShipHandler.getInstance().getCurrentShip() );
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		BTHandler.getInstance().doDisconnect();
	}

	public void onConnectClick( View view ) {
        // If IS_BT_CONNECTED is true, then the connection was successful, and the next one should be to disconnect.
        if ( !Settings.IS_BLUETOOTH_CONNECTED )
            BTHandler.getInstance().doConnect();
        else {
            BTHandler.getInstance().doDisconnect();
        }
	}

    public void onUploadClick( View view ) {
		new AWSHandler( getApplicationContext() ).uploadData( null, true );
	}

	public static ShipDetailActivity getShipDetailContext() {
		return shipDetailContext;
	}
}
