package com.sb2030.loggerapp.ui.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sb2030.loggerapp.R;

/**
 * ShipSelectFragment
 * Used to help out when switching between a tablet and a phone.
 * All this contains is the list of available loggers.
 */
public class ShipSelectFrag extends Fragment {

    /**
     * Constructor
     * This is required for Fragment. Don't know why.
     */
    public ShipSelectFrag() {}

    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState ) {
        // Inflate the layout for this fragment
        return inflater.inflate( R.layout.fragment_ship_select, container, false );
    }

    @Override
    public void onViewCreated( @NonNull View view, @Nullable Bundle savedInstanceState ) {
        super.onViewCreated( view, savedInstanceState );
    }
}
