package com.sb2030.loggerapp.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;

import com.google.android.material.navigation.NavigationView;
import com.sb2030.loggerapp.R;
import com.sb2030.loggerapp.data.types.LogMessage;
import com.sb2030.loggerapp.resources.Log;
import com.sb2030.loggerapp.resources.Settings;

public class SettingsActivity extends AppCompatActivity {

    // For the action bar
    ActionBarDrawerToggle toggle;

    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_settings );

        getSupportFragmentManager()
                .beginTransaction()
                .replace( R.id.settings, new SettingsFragment() )
                .commit();

        // And now let's set up the slide out drawer stuff
        DrawerLayout dl = findViewById( R.id.settings_page_layout );

        // Private class declarations for the toggle bar thing
        toggle = new ActionBarDrawerToggle( this, dl, R.string.drawer_open_text, R.string.drawer_close_text );

        // This adds the toggling methods, and gives a way to sync up the states when you
        // rotate the UI. Note: Each layout xml that utilizes a drawer must have a
        // com.google.android.material.navigation.NavigationView component to it that has
        // android:layout_gravity="start" set. Or else you'll crash.
        dl.addDrawerListener( toggle );
        toggle.syncState();

        // This sets up the cheeseburger at the top left.
        if ( getSupportActionBar() != null )
            getSupportActionBar().setDisplayHomeAsUpEnabled( true );
        else
            Log.log( new LogMessage( getClass().getSimpleName(), LogMessage.MESSAGE_TYPE.ERROR,
                    "Could not get the support action bar." ) );

        // Get the navigation view, which is the slide out drawer.
        NavigationView nv = findViewById( R.id.nav_view_settings );

        // Here we'll actually handle what the user has selected. This will make a call to intent
        // subscribers ( which is just MainActivity right now ) to do the navigation
        // The navigation should be changed to an actual nav_path setup, but for right now I think
        // this works fine. Of course, let's not let temp hacks become permanent.
        nv.setNavigationItemSelectedListener( new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected( @NonNull MenuItem item ) {
                int id = item.getItemId();
                switch ( id ) {
                    case R.id.drawer_loggers:
                        switchIntent( 2 );
                        break;
                    case R.id.drawer_jobs:
                        switchIntent( 3 );
                        break;
                    case R.id.drawer_log:
                        switchIntent( 4 );
                        break;
                    case R.id.drawer_settings:
                        switchIntent( 5 );
                        break;
                    default:
                        return true;
                }
                return true;
            }
        } );
    }

    public static class SettingsFragment extends PreferenceFragmentCompat {
        @Override
        public void onCreatePreferences( Bundle savedInstanceState, String rootKey ) {
            setPreferencesFromResource( R.xml.root_preferences, rootKey );

            final String className = "Settings";

            // Now go through each setting and apply a listener for when it changes. These should
            // only affect the Settings.java value.
            findPreference( "show_bt_transactions" ).setOnPreferenceChangeListener( new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange( Preference preference, Object newValue ) {
                    if ( Settings.DEBUG_ON )
                        Log.log( className, LogMessage.MESSAGE_TYPE.DEBUG,
                            "Set Should Show BT Transactions to " + newValue + " from " + Settings.SHOULD_SHOW_BT_TRANSACTIONS );
                    Settings.SHOULD_SHOW_BT_TRANSACTIONS = ( boolean ) newValue;

                    return true;
                }
            } );

            findPreference( "scan_timeout_seconds" ).setOnPreferenceChangeListener( new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange( Preference preference, Object newValue ) {
                    if ( Settings.DEBUG_ON )
                        Log.log( className, LogMessage.MESSAGE_TYPE.DEBUG,
                            "Set Bluetooth Search Timeout to " +  newValue + " from " + Settings.BTLE_SEARCH_TIMEOUT );
                    Settings.BTLE_SEARCH_TIMEOUT = Integer.parseInt( newValue.toString() );

                    return true;
                }
            } );

            findPreference( "debug_level" ).setOnPreferenceChangeListener( new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange( Preference preference, Object newValue ) {
                    // If the debug level is greater than 0
                    if ( Integer.parseInt( ( String ) newValue ) > 0 )
                        Log.log( className, LogMessage.MESSAGE_TYPE.DEBUG,
                            "Set Debug Level to " + newValue );

                    String val = ( String ) newValue;
                    switch ( val ) {
                        case "0":
                            Settings.DEBUG_ON = false;
                            Settings.SUPER_DEBUG_ON = false;
                            break;
                        case "1":
                            Settings.DEBUG_ON = true;
                            Settings.SUPER_DEBUG_ON = false;
                            break;
                        case "2":
                            Settings.DEBUG_ON = true;
                            Settings.SUPER_DEBUG_ON = true;
                            break;
                    }

                    return true;
                }
            } );

            findPreference( "demo_mode_on" ).setOnPreferenceChangeListener( new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange( Preference preference, Object newValue ) {
                    if ( Settings.DEBUG_ON )
                        Log.log( className, LogMessage.MESSAGE_TYPE.DEBUG,
                            "Set Demo Mode to " + newValue + " from " + Settings.DEMO_ON );
                    Settings.DEMO_ON = ( Boolean ) newValue;

                    return true;
                }
            } );

            findPreference( "aws_enable" ).setOnPreferenceChangeListener( new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange( Preference preference, Object newValue ) {
                    if ( Settings.DEBUG_ON )
                        Log.log( className, LogMessage.MESSAGE_TYPE.DEBUG,
                            "Set AWS Upload Enabled to " + newValue + " from " + Settings.DO_AWS );
                    Settings.DO_AWS = ( Boolean ) newValue;

                    return true;
                }
            } );

            findPreference( "log_entries_to_show" ).setOnPreferenceChangeListener( new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange( Preference preference, Object newValue ) {
                    if ( Settings.DEBUG_ON )
                        Log.log( className, LogMessage.MESSAGE_TYPE.DEBUG,
                            "Set Log Entries Shown to " + newValue + " from " + Settings.LOG_ENTRIES_TO_SHOW );
                    Settings.LOG_ENTRIES_TO_SHOW = Integer.parseInt( newValue.toString() );

                    return true;
                }
            } );
        }
    }

    public void switchIntent( int which ) {
        Intent i = null;
        switch ( which ) {
            case 2:
                i = new Intent( this, MainViewActivity.class );
                break;
            case 3:
                i = new Intent( this, JobActivity.class );
                break;
            case 4:
                i = new Intent( this, LogActivity.class );
                break;
            case 5:
                return;
            default: Log.log( new LogMessage( getClass().getSimpleName(),  LogMessage.MESSAGE_TYPE.ERROR, "Invalid ID" ) );
        }
        startActivity( i );
    }

    /**
     * onOptionsItemSelected
     *
     * @param item the option item that was selected. This is the drawer item.
     * @return I'm not sure.
     */
    @Override
    public boolean onOptionsItemSelected( @NonNull MenuItem item ) {
        if ( toggle.onOptionsItemSelected( item ) )
            return true;
        return super.onOptionsItemSelected( item );
    }
}