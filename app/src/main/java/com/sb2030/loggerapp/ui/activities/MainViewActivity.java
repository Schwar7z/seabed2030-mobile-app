package com.sb2030.loggerapp.ui.activities;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Environment;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.preference.PreferenceManager;

import com.google.android.material.navigation.NavigationView;
import com.sb2030.loggerapp.R;
import com.sb2030.loggerapp.data.data_handlers.AWSHandler;
import com.sb2030.loggerapp.data.data_handlers.BTHandler;
import com.sb2030.loggerapp.data.types.LogMessage;
import com.sb2030.loggerapp.messaging.btstatus.BluetoothStatusMessenger;
import com.sb2030.loggerapp.messaging.btstatus.BluetoothStatusSubscriber;
import com.sb2030.loggerapp.messaging.toast.ToastMessenger;
import com.sb2030.loggerapp.messaging.toast.ToastSubscriber;
import com.sb2030.loggerapp.resources.Log;
import com.sb2030.loggerapp.resources.Settings;

import java.io.File;

/**
 * MainViewActivity
 * Originally MainActivity
 * This is the main page a user sees when they load the app up.
 */

// TODO should I make an extendable interface that has the nav drawer stuff in it?
// TODO change everything to be fragments. There's no need to have activities for everything.
public class MainViewActivity extends AppCompatActivity implements
        ToastSubscriber,
        BluetoothStatusSubscriber {

    // In case a non activity class needs context, they get this.
    private static MainViewActivity mainContext;

    // Private class declarations for the toggle bar thing
    private ActionBarDrawerToggle toggle;

    // AWS Stuff
    private AWSHandler awsHandler;

    /**
     * OnCreate
     * This is the entry point for this Activity.
     * It currently sets up a ShipHandler and a LoggerHandler every time it is created.
     * This will have to be either changed into a Facade-like structure, so it doesn't
     * get recreated every time this is loaded, but this works for now.
     * @param savedInstanceState The bundle of saved information to load when this is loaded.
     */
    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_ship_view );

        // Set the context to this. This is accessible to other classes.
        mainContext = this;

        // Tell the IS_PORTRAIT field whether or not we are in portrait or not.
        // This helps us use more screen real estate in tablet mode, or just on larger screens.
        // Set this first so everything below can use it.
        Settings.IS_PORTRAIT = getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT;

        // Subscribe to toast events
        ToastMessenger.subscribe( this );

        // Subscribe to Bluetooth status events
        BluetoothStatusMessenger.subscribe( this );

        // Now let's do setup once and only once.
        if ( !Settings.HAS_MAIN_SET_UP ) {
            // Check if we have app permissions
            checkForPermissions();

            // Do setup for the BT handler.
            BTHandler.getInstance().setup();

            // And initiate the AWS handler with this context.
            awsHandler = new AWSHandler( this );

            // Verify the preferences are stored correctly, since I'm using Settings, and
            // sharedPreferences are stored continuously.
            Settings.sync( PreferenceManager.getDefaultSharedPreferences( this ) );

            // Now set the switch to be closed.
            Settings.HAS_MAIN_SET_UP = true;
        }

        // And now let's set up the slide out drawer stuff
        DrawerLayout dl = findViewById( R.id.ship_page_layout );
        toggle = new ActionBarDrawerToggle( this, dl, R.string.drawer_open_text, R.string.drawer_close_text );

        // This adds the toggling methods, and gives a way to sync up the states when you
        // rotate the UI. Note: Each layout xml that utilizes a drawer must have a
        // com.google.android.material.navigation.NavigationView component to it that has
        // android:layout_gravity="start" set. Or else you'll crash.
        dl.addDrawerListener( toggle );
        toggle.syncState();

        // This sets up the cheeseburger at the top left.
        if ( getSupportActionBar() != null )
            getSupportActionBar().setDisplayHomeAsUpEnabled( true );
        else
            Log.log( new LogMessage( getClass().getSimpleName(), LogMessage.MESSAGE_TYPE.ERROR,
                    "Could not get the support action bar." ) );

        // Get the navigation view, which is the slide out drawer.
        NavigationView nv = findViewById( R.id.nav_view );

        // Here we'll actually handle what the user has selected.
        nv.setNavigationItemSelectedListener( new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected( @NonNull MenuItem item ) {
                int id = item.getItemId();
                switch ( id ) {
                    case R.id.drawer_loggers:
                        switchIntent( 2 );
                        break;
                    case R.id.drawer_jobs:
                        switchIntent( 3 );
                        break;
                    case R.id.drawer_log:
                        switchIntent( 4 );
                        break;
                    case R.id.drawer_settings:
                        switchIntent( 5 );
                        break;
                    default:
                        return true;
                }
                return true;
            }
        } );
    }

    /**
     * CheckForPermissions
     * Checks if we have coarse, fine, and read external permissions
     */
    private void checkForPermissions() {
        // Check if we have COARSE and FINE location
        if ( ContextCompat.checkSelfPermission( this, Manifest.permission.ACCESS_FINE_LOCATION ) != PackageManager.PERMISSION_GRANTED ) {
            if ( ActivityCompat.shouldShowRequestPermissionRationale( this, Manifest.permission.ACCESS_FINE_LOCATION ) ) {
                Log.log( getClass().getSimpleName(), LogMessage.MESSAGE_TYPE.WARNING, "You need to allow this app to have BLE permissions. This includes allowing location." );
            }
            requestPermissions( new String[]{ Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION }, 1 );
        }

        // Same thing, but for reading storage items
        if ( checkSelfPermission( Manifest.permission.READ_EXTERNAL_STORAGE ) != PackageManager.PERMISSION_GRANTED ) {
            if ( shouldShowRequestPermissionRationale( Manifest.permission.READ_EXTERNAL_STORAGE ) ) {
                Log.log( getClass().getSimpleName(), LogMessage.MESSAGE_TYPE.WARNING, "You need to allow this app to have Storage permissions." );
            }
            requestPermissions( new String[]{ Manifest.permission.READ_EXTERNAL_STORAGE }, 2 );
        }

        // Now verify that Bluetooth LE is supported. This should be a thing anyways, since
        // we require it on the device, but it's always worth a check.
        if ( !getPackageManager().hasSystemFeature( PackageManager.FEATURE_BLUETOOTH_LE ) ) {
            Toast.makeText( this, R.string.ble_not_supported, Toast.LENGTH_SHORT ).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        BTHandler.getInstance().doResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        BTHandler.getInstance().doPause();
    }

    /**
     * onOptionsItemSelected
     *
     * @param item the option item that was selected. This is the drawer item.
     * @return I'm not sure.
     */
    @Override
    public boolean onOptionsItemSelected( @NonNull MenuItem item ) {
        if ( toggle.onOptionsItemSelected( item ) )
            return true;
        return super.onOptionsItemSelected( item );
    }

    public void onRefreshButtonClick( View view ) {
        BTHandler.getInstance().scanForDevices( true );
    }

    /**
     * onConnectClick
     * Same function as the method with the same name in ShipDetailFrag, just here
     *
     * @param view The view?
     */
    public void onConnectClick( View view ) {
        // If IS_BT_CONNECTED is true, then the connection was successful,
        // and the next one should be to disconnect.
        // The Setting value updates in these methods.
        if ( !Settings.IS_BLUETOOTH_CONNECTED )
            BTHandler.getInstance().doConnect();
        else
            BTHandler.getInstance().doDisconnect();
    }

    /**
     * onUploadClick
     * Same function as the method with the same name in ShipDetailFrag, just here
     *
     * @param view The view this layout uses. I think. I don't really know. I'm tired and want coffee.
     */
    public void onUploadClick( View view ) {
        File sdcard = Environment.getExternalStorageDirectory();
        final File toRead = new File( sdcard, "/My Documents/geojson_example_from_app.json" );

        Thread thread = new Thread( new Runnable() {
            @Override
            public void run() {
                try {
                    boolean success = awsHandler.uploadData( toRead, Settings.DO_AWS );
                    if ( !success ) {
                        if ( !Settings.DO_AWS )
                            Log.log( "MainActivity", LogMessage.MESSAGE_TYPE.WARNING,
                                    "No submission was processed because of demo or deactivated AWS subs" );
                        else
                            Log.log( "MainActivity", LogMessage.MESSAGE_TYPE.ERROR,
                                    "Could not submit to AWS, as an error occurred." );
                    }
                } catch ( Exception e ) {
                    e.printStackTrace();
                }
            }
        } );
        thread.start();

        // I'm going to temporarily use this to print out the preferences.
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences( this );
        Log.log( getClass().getSimpleName(), LogMessage.MESSAGE_TYPE.INFORMATION, "Settings: " + sp.getAll().toString() );
    }

    public static MainViewActivity getMainContext() {
        return mainContext;
    }

    /**
     * DisplayToast
     * Displays a toast message sent by anywhere else in the app.
     * This is enabled by extending the ToastSubscriber interface, and by
     * subscribing to ToastMessenger.
     *
     * @param msg The message to display
     */
    @Override
    public void displayToast( String msg ) {
        Toast.makeText( getApplicationContext(), msg, Toast.LENGTH_SHORT ).show();
    }

    /**
     * UpdateBluetoothStatus
     * Updates any target on this activity with the msg about the Bluetooth connection status
     * specified by LoggerHandler.java
     * This is enabled by extending the BluetoothStatusSubscriber interface, and by subscribing to
     * BluetoothStatusMessenger
     *
     * @param msg The message to display
     */
    @Override
    public void updateBluetoothStatus( String msg ) {
        // Display the status of the bluetooth connection in some label or something here
        Resources r = getResources();
        String bluetoothStatus = r.getString( R.string.bluetooth_status_text, msg );
        TextView lbl = findViewById( R.id.connections_header );
        lbl.setText( bluetoothStatus );
    }

    public void switchIntent( int which ) {
        Intent i = null;
        switch ( which ) {
            case 1:
                i = new Intent( this, ShipDetailActivity.class );
                break;
            case 2:
                i = new Intent( this, MainViewActivity.class );
                break;
            case 3:
                i = new Intent( this, JobActivity.class );
                break;
            case 4:
                i = new Intent( this, LogActivity.class );
                break;
            case 5:
                i = new Intent( this, SettingsActivity.class );
                break;
            default:
                Log.log( new LogMessage( getClass().getSimpleName(), LogMessage.MESSAGE_TYPE.ERROR, "Invalid ID" ) );
        }
        if ( i != null ) {
            // First things first, unsubscribe from everything
            ToastMessenger.unsubscribe( this );
            BluetoothStatusMessenger.unsubscribe( this );

            // Then go to the activity
            startActivity( i );
        }
    }
}