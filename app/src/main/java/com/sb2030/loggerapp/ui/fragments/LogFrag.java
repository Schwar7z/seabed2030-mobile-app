package com.sb2030.loggerapp.ui.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sb2030.loggerapp.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link LogFrag#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LogFrag extends Fragment {

    public LogFrag() {}

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment LogFrag.
     */
    public static LogFrag newInstance(String param1, String param2) {
        LogFrag fragment = new LogFrag();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_log_fragmet, container, false);
    }
}