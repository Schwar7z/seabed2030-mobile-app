package com.sb2030.loggerapp.ui.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sb2030.loggerapp.R;
import com.sb2030.loggerapp.data.types.LogMessage;
import com.sb2030.loggerapp.resources.Log;

/**
 * LogItemFrag
 * This is what encapsulates a log item. This is the thing you're reading from when in the log
 * viewer
 */
public class LogItemFrag extends Fragment {

    public LogItemFrag() {}

    @Override
    public View onCreateView( @NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState ) {
        // Inflate the layout for this fragment
        return inflater.inflate( R.layout.fragment_log_item, container, false );
    }

    @Override
    public void onViewCreated( @NonNull View view, @Nullable Bundle savedInstanceState ) {
        super.onViewCreated( view, savedInstanceState );

        if ( getArguments() == null ) {
            Log.log( new LogMessage( "LogItemFrag", LogMessage.MESSAGE_TYPE.ERROR,
                    "There is no bundle for the log item fragment. This is definitely bad." ) );
        } else {
            // Now we set the log item to represent what it actually is.
            int logType = getArguments().getInt( "log_type" );
            String fromWhere = getArguments().getString( "source" );
            String message = getArguments().getString( "message" );

            TextView header = view.findViewById( R.id.text_log_header );

            AppCompatImageView icon = view.findViewById( R.id.image_log_message_type );
            switch ( logType ) {
                case 0:
                    icon.setImageResource( R.drawable.ic_message_black_24dp );
                    header.setText( R.string.log_header_info_text );
                    break;
                case 1:
                    icon.setImageResource( R.drawable.ic_warning_black_24dp );
                    header.setText( R.string.log_header_warn_text );
                    break;
                case 2:
                    icon.setImageResource( R.drawable.ic_error_black_24dp );
                    header.setText( R.string.log_header_error_text );
                    break;
                case 3:
                    icon.setImageResource( R.drawable.ic_bug_report_black_24dp );
                    header.setText( R.string.log_header_debug_text );
                    break;
                default:
                    icon.setImageResource( R.drawable.ic_close_black_24dp );
                    header.setText( R.string.log_header_default_text );
            }

            TextView sourceField = view.findViewById( R.id.text_source );
            sourceField.setText( fromWhere );

            TextView messageField = view.findViewById( R.id.text_message );

            if ( message == null )
                message = "";

            String[] splitMessage = message.split( "\n" );
            String newMessage = message;
            if ( splitMessage.length != 1 ) {
                StringBuilder sb = new StringBuilder();
                for ( String s: splitMessage ) {
                    sb.append( s );
                    sb.append( "\n" );
                }
                newMessage = sb.toString();
            }
            messageField.setText( newMessage );
        }
    }

}
