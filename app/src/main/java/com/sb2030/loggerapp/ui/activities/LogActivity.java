package com.sb2030.loggerapp.ui.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.navigation.NavigationView;

import com.sb2030.loggerapp.R;
import com.sb2030.loggerapp.data.types.LogMessage;
import com.sb2030.loggerapp.ui.fragments.LogItemFrag;
import com.sb2030.loggerapp.messaging.log.LogMessenger;
import com.sb2030.loggerapp.messaging.log.LogSubscriber;
import com.sb2030.loggerapp.resources.Log;

import java.util.ArrayList;

public class LogActivity extends AppCompatActivity implements LogSubscriber {

    // For the action bar
    ActionBarDrawerToggle toggle;

    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_log );

        // And now let's set up the slide out drawer stuff
        DrawerLayout dl = findViewById( R.id.log_page_layout );

        // Private class declarations for the toggle bar thing
        toggle = new ActionBarDrawerToggle( this, dl, R.string.drawer_open_text, R.string.drawer_close_text );

        // This adds the toggling methods, and gives a way to sync up the states when you
        // rotate the UI. Note: Each layout xml that utilizes a drawer must have a
        // com.google.android.material.navigation.NavigationView component to it that has
        // android:layout_gravity="start" set. Or else you'll crash.
        dl.addDrawerListener( toggle );
        toggle.syncState();

        // This sets up the cheeseburger at the top left.
        if ( getSupportActionBar() != null )
            getSupportActionBar().setDisplayHomeAsUpEnabled( true );
        else
            Log.log( new LogMessage( getClass().getSimpleName(), LogMessage.MESSAGE_TYPE.ERROR,
                    "Could not get the support action bar." ) );

        // Get the navigation view, which is the slide out drawer.
        NavigationView nv = findViewById( R.id.nav_view_log );

        // Here we'll actually handle what the user has selected. This will make a call to intent
        // subscribers ( which is just MainActivity right now ) to do the navigation
        // The navigation should be changed to an actual nav_path setup, but for right now I think
        // this works fine. Of course, let's not let temp hacks become permanent.
        nv.setNavigationItemSelectedListener( new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected( @NonNull MenuItem item ) {
                int id = item.getItemId();
                switch ( id ) {
                    case R.id.drawer_loggers:
                        switchIntent( 2 );
                        break;
                    case R.id.drawer_jobs:
                        switchIntent( 3 );
                        break;
                    case R.id.drawer_log:
                        switchIntent( 4 );
                        break;
                    case R.id.drawer_settings:
                        switchIntent( 5 );
                        break;
                    default:
                        return true;
                }
                return true;
            }
        } );

        LogMessenger.subscribe( this );
        LogMessenger.update( Log.getAllLogs() );
    }

    /**
     * NewUpdatesAvailable
     * Tells this to grab the new log entries from the Log class
     */
    @Override
    public void newUpdatesAvailable( ArrayList< LogMessage > messages ) {
        for ( LogMessage l : messages ) {
            LogItemFrag lif = new LogItemFrag();

            // The type of message. You could probably just use the MESSAGE_TYPE, but ya know
            // it's 0004 and I'm tired.
            int type;
            switch ( l.getType() ) {
                case INFORMATION:
                    type = 0;
                    break;
                case WARNING:
                    type = 1;
                    break;
                case ERROR:
                    type = 2;
                    break;
                case DEBUG:
                    type = 3;
                    break;
                default:
                    type = -1;
            }

            Bundle b = new Bundle();
            b.putInt( "log_type", type );
            b.putString( "source", l.getSource() );
            b.putString( "message", l.getMessage() );
            lif.setArguments( b );

            try {
                getSupportFragmentManager().beginTransaction().add( R.id.message_frag_container, lif ).commit();
            } catch ( IllegalStateException ise ) {
                // Ignore this. It's a bug
            }
        }
    }

    public void switchIntent( int which ) {
        Intent i = null;
        switch ( which ) {
            case 2:
                i = new Intent( this, MainViewActivity.class );
                break;
            case 3:
                i = new Intent( this, JobActivity.class );
                break;
            case 4:
                return;
            case 5:
                i = new Intent( this, SettingsActivity.class );
                break;
            default:
                Log.log( new LogMessage( getClass().getSimpleName(), LogMessage.MESSAGE_TYPE.ERROR, "Invalid ID" ) );
        }
        startActivity( i );
    }

    /**
     * onOptionsItemSelected
     *
     * @param item the option item that was selected. This is the drawer item.
     * @return I'm not sure.
     */
    @Override
    public boolean onOptionsItemSelected( @NonNull MenuItem item ) {
        if ( toggle.onOptionsItemSelected( item ) )
            return true;
        return super.onOptionsItemSelected( item );
    }
}
