package com.sb2030.loggerapp.data.types;

import android.bluetooth.BluetoothDevice;
import android.os.ParcelUuid;

import androidx.annotation.NonNull;

import com.sb2030.loggerapp.resources.Log;
import com.sb2030.loggerapp.resources.Settings;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * The ship class
 * This encapsulates all of the data about visible ships
 * Visible = connected via bluetooth
 */
public class Ship {

    // The private variables describing the boat / logger.
    private String id;
    private String name;
    private long lastCheckInTime;
    private String data;
    private ArrayList< String > gattData;
    private BluetoothDevice device;

    // Non informative flags
    private boolean isDisplayed = false;

    /**
     * Constructor to initialize the data members with whatever is passed
     *
     * @param device The device this "ship" represents.
     */
    public Ship( BluetoothDevice device ) {
        this.device = device;
        this.id = device.getAddress();
        this.name = device.getName();
        this.lastCheckInTime = System.currentTimeMillis();

        this.gattData = new ArrayList<>();
        fillData();
    }

    /**
     * Constructor for demo
     * @param name The name of the ship
     */
    public Ship( String id, String name ) {
        this.id = id;
        this.name = name;
        this.lastCheckInTime = System.currentTimeMillis();
        fillData();
    }


    private void fillData() {
        StringBuilder data = new StringBuilder();
        data.append( "Device Name: " );
        data.append( this.device.getName() );
        data.append( "\nAddress: " );
        data.append( this.device.getAddress() );
        data.append( "\nType: " );
        data.append( this.device.getType() );
        data.append( "\nClass: " );
        data.append( this.device.getBluetoothClass() );
        data.append( "\nUUID(s): " );

        if ( this.device.getUuids() != null ) {
            int counter = 0;
            for ( ParcelUuid uuid : this.device.getUuids() ) {
                data.append( "\n   " );
                data.append( ++counter );
                data.append( ": " );
                data.append( uuid );
            }
        }
        else
            data.append( "None." );

        this.data = data.toString();
    }

    /**
     * GetDevice
     * @return The bluetooth device associated with this "ship"
     */
    public BluetoothDevice getDevice() {
        return this.device;
    }

    // Getters and setters for the data fields.
    public String getLastCheckInTime() {
        Date d = new Date( lastCheckInTime );
        SimpleDateFormat sdf = new SimpleDateFormat( "MM/dd/YYYY - HH:mm", Locale.US );
        return sdf.format( d );
    }

    public void setLastCheckInTime( Long lastCheckInTime ) {
        this.lastCheckInTime = lastCheckInTime;
    }

    public String getName() {
        return name;
    }

    public void setName( String name ) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId( String id ) {
        this.id = id;
    }

    public boolean isDisplayed() {
        return isDisplayed;
    }

    public void setDisplayed( boolean displayed ) {
        isDisplayed = displayed;
    }

    public String getData() {
        return data;
    }

    public void setData( String data ) {
        this.data = data;
    }

    public ArrayList<String> getGattData() { return gattData; }

    public void updateGattData( String update ) { gattData.add( update ); }
    /**
     * Overridden toString
     * Returns the Ship's info in an easy to read format
     *
     * @return Ship Info
     */
    @Override
    @NonNull
    public String toString() {
        if ( Settings.DEBUG_ON )
            return getName() + ": " + getId();
        else
            return getName();
    }

    @Override
    public boolean equals( Object o ) {
        if ( o instanceof Ship ) {
            if ( ( ( Ship ) o ).id.equals( this.id ) ) {
                return ( ( Ship ) o ).name.equals( this.name );
            }
        }
        return false;
    }


}
