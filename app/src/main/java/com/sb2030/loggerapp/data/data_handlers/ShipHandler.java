package com.sb2030.loggerapp.data.data_handlers;

import com.sb2030.loggerapp.data.types.LogMessage;
import com.sb2030.loggerapp.data.types.Ship;
import com.sb2030.loggerapp.resources.Log;
import com.sb2030.loggerapp.sample.SampleBoatData;

import java.util.ArrayList;
import java.util.Arrays;

public class ShipHandler {

	private ArrayList< Ship > shipList;
	private Ship currentShip;

	private static ShipHandler instance;

	public static ShipHandler getInstance() {
		if ( instance == null )
			instance = new ShipHandler();
		return instance;
	}

	private ShipHandler() {
		this.shipList = new ArrayList<>();
	}

	/**
	 * getShipById
	 *
	 * @return The ship you requested by ID, sir. Careful, for she may be a little rusty in parts.
	 */
	public Ship getShipById( String ID ) {
		for ( Ship s : this.shipList ) {
			if ( s.getId().equalsIgnoreCase( ID ) )
				return s;
		}
		return null;
	}

	/**
	 * Has
	 * If the ship list has the ships ID, return true.
	 * @param ID The ID to search for
	 * @return If the ship exists or not
	 */
	public boolean has( String ID ) {
		for ( Ship s : this.shipList ) {
			if ( s.getId().equalsIgnoreCase( ID ) )
				return true;
		}
		return false;
	}

	private int getNumberConnectedShips() {
		return this.shipList.size();
	}

	/**
	 * AddShip
	 * Adds a currently connected ship into the system
	 *
	 * @param s The ship to add
	 * @param uniqueOnly Whether or not a ship won't be added if it is already on the list.
	 * @return boolean Always true if uniqueOnly is false
	 * 				   false when uniqueOnly is true and the ship exist
	 */
	public boolean addShip( Ship s, boolean uniqueOnly ) {
		if ( uniqueOnly ) {
			if ( !this.shipList.contains( s ) ) {
				this.shipList.add( s );
				return true;
			}
			return false;
		}
		this.shipList.add( s );
		return false;
	}

	/**
	 * AddBatchShips
	 * This should be called when adding multiple ships at once.
	 * This prevents displaying all ships every time you add a single ship.
	 *
	 * @param s The ships to add
	 */
	private void addBatchShips( Ship[] s ) {
		this.shipList.addAll( Arrays.asList( s ) );
	}

	public ArrayList< Ship> getAllShips() {
		return this.shipList;
	}

	public void setCurrentShip( Ship s ) {
		this.currentShip = s;
	}

	public Ship getCurrentShip() {
		return this.currentShip;
	}

	/**
	 * RemoveShip
	 * Removes a ship from the list. Usually happens on disconnect
	 *
	 * @param s The ship to remove
	 * @return Whether or not it was successful. This should always be true if the ship exists
	 */
	public boolean removeShip( Ship s ) {
		// TODO: remove a ship from the entry list
		return shipList.remove( s );
	}

	/**
	 * Remove all ships in case of
	 */
	public void removeAllShips() {
		shipList.clear();
	}

	/**
	 * ResetAllShipsDisplayedStatus
	 * This sets each ship to not being displayed, so when you call display... then it'll display
	 * all again.
	 */
	public void resetAllShipsDisplayedStatus() {
		for ( Ship s : this.shipList ) {
			s.setDisplayed( false );
		}
	}

	/**
	 * Do Sample
	 * Purely for sampling the data. Isn't actually called in production
	 *
	 * @param amount The amount of ships to add
	 */
	public void doSample( int amount ) {
		Log.log( new LogMessage( getClass().getSimpleName(), LogMessage.MESSAGE_TYPE.INFORMATION,"Creating sample data with " + amount + " ships..." ) );
		Ship[] a = SampleBoatData.getSampleConnection( amount );
		addBatchShips( a );
	}
}
