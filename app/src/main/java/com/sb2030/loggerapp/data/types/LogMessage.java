package com.sb2030.loggerapp.data.types;

/**
 * LogMessage
 * A wrapper class for log messages, so that they can be viewed on the Log view
 */
public class LogMessage {
    // An enum to declare the types.
    public enum MESSAGE_TYPE { INFORMATION, WARNING, ERROR, DEBUG }

    // The parts of the message
    private String source;
    private MESSAGE_TYPE type;
    private String message;

    /**
     * Default constructor
     * @param source The source of the message ( "bt", "aws", "general", etc )
     * @param type The type of message, as following the Enum
     * @param message The actual message
     */
    public LogMessage( String source, MESSAGE_TYPE type, String message ) {
        this.source = source;
        this.type = type;
        this.message = message;
    }

    /**
     * GetSource
     * @return The source of the log message
     */
    public String getSource(){
        return this.source;
    }

    /**
     * GetType
     * @return The type of the message, as a MESSAGE_TYPE enum
     */
    public MESSAGE_TYPE getType() {
        return type;
    }

    /**
     * GetMessage
     * @return The actual message
     */
    public String getMessage(){
        return this.message;
    }
}
