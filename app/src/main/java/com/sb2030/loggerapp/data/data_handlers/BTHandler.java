package com.sb2030.loggerapp.data.data_handlers;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.content.Intent;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.sb2030.loggerapp.R;
import com.sb2030.loggerapp.ui.activities.MainViewActivity;
import com.sb2030.loggerapp.ui.activities.ShipDetailActivity;
import com.sb2030.loggerapp.data.types.LogMessage;
import com.sb2030.loggerapp.data.types.Ship;
import com.sb2030.loggerapp.messaging.btstatus.BluetoothStatusMessenger;
import com.sb2030.loggerapp.messaging.ship.ShipMessenger;
import com.sb2030.loggerapp.resources.Log;
import com.sb2030.loggerapp.resources.Settings;

public class BTHandler {

    // Bluetooth related items
    private boolean isBTStarted = false;
    private BluetoothAdapter btAdapter;
    private Handler handler = new Handler();
    private BluetoothLeScanner btLEScanner;

    // Timer related Items
    private Handler timerHandler;
    private Runnable timerRunnable;

    private static BTHandler instance;

    /**
     * GetInstance
     *
     * @return The instance for a Singleton.
     */
    public static BTHandler getInstance() {
        if ( instance == null )
            instance = new BTHandler();
        return instance;
    }

    /**
     * Setup
     * The initial set up of variables to be used later, as well as events.
     */
    public void setup() {
        // If the bluetooth is already set up, then ignore.
        if ( Settings.IS_BLUETOOTH_ON )
            return;

        // Set up BT adapters.
        btAdapter = BluetoothAdapter.getDefaultAdapter();

        if ( btAdapter == null ) {
            Log.log( new LogMessage( getClass().getSimpleName(), LogMessage.MESSAGE_TYPE.ERROR, "Bluetooth adapter can not be started. Are you in a VM?" ) );
            Settings.IS_IN_VM = true;
            return;
        }

        btLEScanner = btAdapter.getBluetoothLeScanner();

        // Timer bar related items
        timerHandler = new Handler();
        timerRunnable = new Runnable() {
            @Override
            public void run() {
                ProgressBar pb = MainViewActivity.getMainContext().findViewById( R.id.pbSearchTimeLeft );
                if ( pb != null ) {
                    int increment = 100 / ( Settings.BTLE_SEARCH_TIMEOUT / 1000 );

                    pb.setProgress( pb.getProgress() + increment, true );

                    timerHandler.postDelayed( this, 1000 );
                }
            }
        };

        Settings.IS_BLUETOOTH_ON = true;

        Log.log( new LogMessage( getClass().getSimpleName(), LogMessage.MESSAGE_TYPE.INFORMATION, "Bluetooth set up" ) );
    }

    /**
     * DoPause
     * An extension of the MainViewActivity's onPause. This is called from there.
     */
    public void doPause() {
        if ( !Settings.IS_IN_VM ) {
            stopScan();
            ShipHandler.getInstance().resetAllShipsDisplayedStatus();
        }
    }

    /**
     * DoResume
     * An extension of the MainViewActivity's onResume. This is called from there.
     */
    public void doResume() {
        if ( !Settings.IS_IN_VM ) {
            clearShipList();
            switchUIToReflectScanningMode( 1 );
            showAllShipsOnList();
            BluetoothStatusMessenger.send( "Ready to search" );

            if ( ShipHandler.getInstance().getCurrentShip() != null )
                ShipMessenger.send( ShipHandler.getInstance().getCurrentShip() );
        }
    }

    /**
     * ScanForDevices
     * This will scan for BTle devices
     */
    public void scanForDevices( boolean search ) {
        if ( Settings.IS_IN_VM )
            return;

        // If the bluetooth system hasn't been started, start it and ask for bluetooth permissions.
        if ( !isBTStarted ) {
            // Check to see what the deal with BT is, and if it's not enabled, try to enable it.
            if ( btAdapter == null || !btAdapter.isEnabled() ) {
                BluetoothStatusMessenger.send( "Requesting BT Enable..." );

                Intent enableBtIntent = new Intent( BluetoothAdapter.ACTION_REQUEST_ENABLE );
                MainViewActivity.getMainContext().startActivityForResult( enableBtIntent, 1 );
            }

            isBTStarted = true;

            // Update the status to reflect what is happening in the code.
            BluetoothStatusMessenger.send( "Ready to search" );
        }
        switchUIToReflectScanningMode( 0 );

        if ( Settings.DEMO_ON ) {
            // Just do demo stuff.
            ShipHandler.getInstance().doSample( 3 );
        } else {
            // Do a BTLe scan.
            if ( search ) {
                // Stops scanning after a pre-defined scan period.
                handler.postDelayed( new Runnable() {
                    @Override
                    public void run() {
                        btLEScanner.stopScan( leScanCallback );
                        switchUIToReflectScanningMode( 1 );
                    }
                }, Settings.BTLE_SEARCH_TIMEOUT );
                btLEScanner.startScan( leScanCallback );

                // Update the header text. You could just call the method itself, but this
                // allows for Find Usages to index this spot, and it's just proper.
                BluetoothStatusMessenger.send( "Searching..." );
            } else {
                Log.log( new LogMessage( getClass().getSimpleName(), LogMessage.MESSAGE_TYPE.INFORMATION, "Stopping btLE scan" ) );
                btLEScanner.stopScan( leScanCallback );
            }
        }
    }

    /**
     * Forcibly stops the LE scan. This is used in the onPause method of MainViewActivity
     */
    private void stopScan() {
        if ( !Settings.IS_IN_VM )
            btLEScanner.stopScan( leScanCallback );
    }

    // Necessary for the le scan
    // Any time a device is found, this gets called from the method above it, and a new device
    // is added to the list.
    private ScanCallback leScanCallback = new ScanCallback() {
        @Override
        public void onScanResult( int callbackType, ScanResult result ) {

            // If the device is null, return.
            if ( result.getDevice().getName() == null )
                return;

            // If the user has specified results filtering, only show devices with "logger" in the
            // name. Otherwise, just add them to the list. By default this is false.
            if ( Settings.FILTER_BTLE_SEARCH_RESULTS ) {
                if ( result.getDevice().getName().toLowerCase().contains( "logger" ) )
                    addShipToList( result.getDevice() );
            } else
                addShipToList( result.getDevice() );
        }
    };

    /**
     * AddShipToList
     * A helper function for the BTLE san callback that just makes a ship with the device,
     * and then adds it to the list if possible.
     *
     * @param device The device to potentially add.
     */
    private void addShipToList( BluetoothDevice device ) {

        // Check to see if the device is already associated with a ship.
        // If it is, then just don't bother making a new ship. Save CPU cycles.
        if ( Settings.ONLY_ADD_UNIQUE_DEVICES_TO_SHIP_LIST ) {
            if ( ShipHandler.getInstance().has( device.getAddress() ) )
                return;
        }

        // Otherwise, create a "Ship" using that device.
        Ship s = new Ship( device );

        // If this returns true, and Settings.ONLY_ADD_UNIQUE_DEVICES_TO_SHIP_LIST is set to true,
        // then the ship / device is in fact unique and is not on the list. The Settings value is
        // set to true by default, because why have multiple entries for the same thing.
        if ( ShipHandler.getInstance().addShip( s, Settings.ONLY_ADD_UNIQUE_DEVICES_TO_SHIP_LIST ) )
            showSingleShip( s );
    }


    /**
     * ShowAllShips
     * Called on screen refresh to redisplay all of the listed loggers
     */
    private void showAllShipsOnList() {
        for ( Ship s : ShipHandler.getInstance().getAllShips() )
            showSingleShip( s );
    }

    /**
     * ShowSingleShip
     * Adds one ship to the list, while checking if it's already been listed
     *
     * @param s The ship to add.
     */
    private void showSingleShip( Ship s ) {
        if ( s.isDisplayed() )
            return;
        else
            s.setDisplayed( true );

        // Make a new button, and give it the name of the ship / ID of the ship.
        Button newShipButton = new Button( MainViewActivity.getMainContext() );

        // Set the name to the ships name, and the id.
        String name = s.getName() + ": " + s.getId();
        newShipButton.setText( name );

        attachButtonActionListener( newShipButton, s );

        // Create the layout parameters, and give it to the button. Basically, just use the
        // defaults.
        LinearLayout ll = MainViewActivity.getMainContext().findViewById( R.id.frag_ship_selector );
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        ll.addView( newShipButton, lp );
    }

    /**
     * Clear ship list
     * Just removes all of the devices from the device list.
     */
    private void clearShipList() {
        LinearLayout ll = MainViewActivity.getMainContext().findViewById( R.id.frag_ship_selector );
        ll.removeAllViews();
    }

    /**
     * AttachButtonActionListener
     * Gives each button that's generated on the fly an action event, as well as allows it to
     * assign a bit of data that tells which button is firing, and handle that accordingly.
     *
     * @param b    The button to attach the listener to
     * @param meta The meta data to tell which button is doing whatnot
     */
    private void attachButtonActionListener( final Button b, final Ship meta ) {
        b.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick( View v ) {
                // Always set the current ship to the one that was just pressed.
                ShipHandler.getInstance().setCurrentShip( meta );

                if ( Settings.IS_PORTRAIT ) {
                    btLEScanner.stopScan( leScanCallback );

                    // Since we're in portrait, we're in the main activity and
                    // we need to go to ShipDetail, so ask MainView for the context,
                    // and switch.
                    MainViewActivity.getMainContext().startActivity( new Intent(
                            MainViewActivity.getMainContext(),
                            ShipDetailActivity.class )
                    );
                } else {
                    ShipMessenger.send( meta );
                }
            }
        } );
    }

    /**
     * SwitchRefreshButtonText
     * This just handles what the refresh button says, so we don't have to keep searching for the
     * view and whatnot
     *
     * @param mode Which mode should the refresh button be in
     *             0 - There is a scan in motion, so disable the button and make it say "Searching"
     */
    private void switchUIToReflectScanningMode( int mode ) {
        if ( Settings.IS_IN_VM )
            return;

        // Button for refreshes
        final Button refreshButton = MainViewActivity.getMainContext().findViewById( R.id.button_refresh );

        // If we are in portrait, we switch activities when we select the device, so this will be
        // null.
        if ( refreshButton == null )
            return;

        // TODO: Change this so it's update in ShipSelectFrag
        if ( mode == 0 ) {
            // Disable the button so they can't spam it
            refreshButton.setEnabled( false );

            // Set the button to say "Searching..."
            refreshButton.setText( R.string.button_refresh_text_during );

            // And set the progress bar to increase every second
            timerHandler.postDelayed( timerRunnable, 0 );
        } else if ( mode == 1 ) {

            // Set the top header label to "Searched"
            BluetoothStatusMessenger.send( "Searched" );

            // We're ready for another search, unlock the button.
            refreshButton.setEnabled( true );
            refreshButton.setText( R.string.button_refresh_text_after );

            // Set the progress bar to 0, and stop the timer controlling the bar's progress
            timerHandler.removeCallbacks( timerRunnable );

            ProgressBar pb = MainViewActivity.getMainContext().findViewById( R.id.pbSearchTimeLeft );
            pb.setProgress( 0, true );
        }
    }

    /**
     * DoConnect
     * An method to allow other classes to dictate connections.
     */
    public void doConnect() {
        // Call the BTGattHandler, since this is bt LE we're using, to connect to the current device
        boolean res = BTGattHandler.getInstance().connectToServer( ShipHandler.getInstance().getCurrentShip().getDevice() );
        if ( res ) {
            // The connection was successful. Tell the log, and set the Setting.
            Log.log( new LogMessage( getClass().getSimpleName(), LogMessage.MESSAGE_TYPE.INFORMATION,
                    "Connected to " + ShipHandler.getInstance().getCurrentShip().getName() ) );

            Settings.IS_BLUETOOTH_CONNECTED = true;
        }
        else
            Log.log( new LogMessage( getClass().getSimpleName(), LogMessage.MESSAGE_TYPE.ERROR,
                "Could not connect for unknown reasons. Try again." ) );
    }

    /**
     * DoDisconnect
     * This will disconnect from the server.
     */
    public void doDisconnect() {

        // This will send a ShipMessage to the ShipDetailActivity that a disconnect has happened,
        // and it will change it's "Disconnect" text to "Connect to Device"
        BTGattHandler.getInstance().disconnectFromServer();

        // Let the log know, and update the setting.
        Log.log( new LogMessage( getClass().getSimpleName(), LogMessage.MESSAGE_TYPE.INFORMATION,
                "Disconnected from " + ShipHandler.getInstance().getCurrentShip().getName() ) );

        Settings.IS_BLUETOOTH_CONNECTED = false;
    }

}
