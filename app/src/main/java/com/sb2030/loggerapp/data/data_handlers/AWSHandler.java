package com.sb2030.loggerapp.data.data_handlers;

import android.content.Context;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobile.client.Callback;
import com.amazonaws.mobile.client.UserStateDetails;
import com.amazonaws.mobileconnectors.s3.transferutility.*;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;

import com.amazonaws.services.s3.AmazonS3Client;
import com.sb2030.loggerapp.data.types.LogMessage;
import com.sb2030.loggerapp.resources.Log;
import com.sb2030.loggerapp.resources.Settings;

import java.io.File;

/**
 * AWSHandler
 * Handles everything with the S3 upload.
 * Not a singleton, but might be best as one. I did that to avoid have context issues.
 */
public class AWSHandler {

    // The transfer utility that handles the upload.
    private TransferUtility transferUtility;

    public AWSHandler( Context c ) {

        // Check to make sure we have the AWS_ENABLED setting turned on. This'll prevent crashing
        // when you, the developer, doesn't have AWS SDK installed.
        if ( !Settings.AWS_ENABLED ) {
            Log.log( getClass().getSimpleName(), LogMessage.MESSAGE_TYPE.WARNING,
                    "AWS ENABLED setting has been turned off.");
            return;
        }

        // Create a new AWSMobileClient with a callback that responds to the user state.
        // This will report any issues with the account used to log into AWS.
        // This shouldn't error, but an error would look like: "UserStateDetails result: User LOGGED_OUT"
        AWSMobileClient.getInstance().initialize( c, new Callback< UserStateDetails >() {
            @Override
            public void onResult( UserStateDetails result ) {
                System.out.println( "UserStateDetails result: " + result.getUserState() );
            }

            @Override
            public void onError( Exception e ) {
                Log.log( new LogMessage( getClass().getSimpleName(), LogMessage.MESSAGE_TYPE.ERROR,
                        "Error with UserStateDetails: " + e.getMessage() ) );
            }
        } );

        BasicAWSCredentials credentials = new BasicAWSCredentials(
                "AKIAVTLCTRHIKGL7FPWF",
                "6rm/bTVKJZ3W6vahPCQIHlIyM62VPxftp//3FkyR" );

        AmazonS3Client s3Client = new AmazonS3Client(
                credentials,
                Region.getRegion( Regions.US_EAST_2 ) );

        transferUtility = TransferUtility.builder()
                .context( c )
                .awsConfiguration( AWSMobileClient.getInstance().getConfiguration() )
                .s3Client( s3Client )
                .build();

        TransferNetworkLossHandler.getInstance( c );
    }

    public boolean uploadData( File f, boolean shouldUpload ) {
        Log.log( new LogMessage( "AWSHandler", LogMessage.MESSAGE_TYPE.INFORMATION,
                "Uploading information..." ) );
        if ( f == null ) {
            System.err.println( "File is null" );
            return false;
        }
        else if ( !f.exists() ) {
            System.err.println( "File doesn't exist?" );
            return false;
        }
        else if ( f.isDirectory() ) {
            System.err.println( "File is a directory" );
            return false;
        }

        // Basically if we're doing a demo, then just return. The debug info is handy though.
        // We could just upload a dumb file to a demo bucket, but whatever. It's May.
        if ( !shouldUpload )
            return false;

        // This is the function that starts the upload. Everything below is a result from it.
        TransferObserver uploadObserver = transferUtility.upload( "csb-submission", f.getName(), f );

        Log.log( new LogMessage( "AWSHandler", LogMessage.MESSAGE_TYPE.INFORMATION,
                "Bucket: " + uploadObserver.getBucket() ) );

        // Attach a listener to the observer to get state update and progress notifications
        uploadObserver.setTransferListener( new TransferListener() {
            @Override
            public void onStateChanged( int id, TransferState state ) {
                if ( TransferState.COMPLETED == state ) {
                    // Handle a completed upload.
                    Log.log( new LogMessage( "AWSHandler", LogMessage.MESSAGE_TYPE.INFORMATION,
                            "Successfully uploaded file." ) );
                }
            }

            @Override
            public void onProgressChanged( int id, long bytesCurrent, long bytesTotal ) {
                double percent = ( ( double ) bytesCurrent / ( double ) bytesTotal ) * 100;

                System.out.println( "Progress: " + percent + "% of " + bytesTotal + " uploaded." );
            }

            @Override
            public void onError( int id, Exception ex ) {
                // Handle errors
                Log.log( new LogMessage( "AWSHandler", LogMessage.MESSAGE_TYPE.ERROR,
                        "Error uploading the file: " + ex.getMessage() ) );
            }
        } );

        // If you prefer to poll for the data, instead of attaching a
        // listener, check for the state and progress in the observer.
        if ( TransferState.COMPLETED == uploadObserver.getState() ) {
            // Handle a completed upload.
            if ( Settings.DO_AWS && !Settings.DEMO_ON ) {
                Log.log(new LogMessage(getClass().getSimpleName(), LogMessage.MESSAGE_TYPE.INFORMATION,
                        "Successfully uploaded file."));
                return true;
            }
        }
        return false;
    }
}