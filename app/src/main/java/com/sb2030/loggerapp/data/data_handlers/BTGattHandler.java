package com.sb2030.loggerapp.data.data_handlers;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;

import com.sb2030.loggerapp.ui.activities.MainViewActivity;
import com.sb2030.loggerapp.ui.activities.ShipDetailActivity;
import com.sb2030.loggerapp.messaging.btgatt.BluetoothGATTMessenger;
import com.sb2030.loggerapp.resources.Settings;

import java.util.UUID;

public class BTGattHandler {

    private BluetoothGatt btGatt;
    private BluetoothGattCharacteristic characteristic;

    private String ClientCharConfig = "00002902-0000-1000-8000-00805f9b34fb";

    private static BTGattHandler instance;

    static BTGattHandler getInstance() {
        if ( instance == null )
            instance = new BTGattHandler();
        return instance;
    }

    boolean connectToServer( BluetoothDevice d ) {
        BluetoothGATTMessenger.send( "Connecting..." );

        // If we're in portrait, then the ShipDetailActivity is the context.
        if ( !Settings.IS_PORTRAIT )
            btGatt = d.connectGatt( MainViewActivity.getMainContext(), false, bluetoothGattCallback );
        else
            btGatt = d.connectGatt( ShipDetailActivity.getShipDetailContext(), false, bluetoothGattCallback );
        return btGatt.connect();
    }

    void disconnectFromServer() {
        if ( btGatt != null ) {
            btGatt.disconnect();
            btGatt.close();
            btGatt = null;
        }
    }

    // Callback for reading services and characteristics.
    private BluetoothGattCallback bluetoothGattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange( BluetoothGatt gatt, int status, int newState ) {
            if ( newState == BluetoothProfile.STATE_CONNECTED ) {
                BluetoothGATTMessenger.send( "Connected." );

                // Set the notification value to true and receive updates
                /*
                btGatt.setCharacteristicNotification( characteristic, true );
                BluetoothGattDescriptor desc = characteristic.getDescriptor( UUID.fromString( ClientCharConfig ) );
                desc.setValue( BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE );
                gatt.writeDescriptor( desc );
                
                 */

                // Read GATT attributes
                gatt.discoverServices();
            } else if ( newState == BluetoothProfile.STATE_DISCONNECTED ) {
                BluetoothGATTMessenger.send( "Disconnected." );
            }
        }

        @Override
        public void onServicesDiscovered( BluetoothGatt gatt, int status ) {
            if ( status == BluetoothGatt.GATT_SUCCESS ) {
                BluetoothGATTMessenger.send( "Services Discovered. Listing:" );
                for ( BluetoothGattService gattService: gatt.getServices() ) {
                    BluetoothGATTMessenger.send("Service: " + serviceDecoder( gattService.getUuid() ) );
                    for ( BluetoothGattCharacteristic gattChar: gattService.getCharacteristics() ) {
                        BluetoothGATTMessenger.send( "\tCharacteristic: " +
                                characteristicDecoder( gattChar.getUuid() ) );
                        for ( BluetoothGattDescriptor desc: gattChar.getDescriptors() )
                            gatt.readDescriptor( desc );
                        gatt.readCharacteristic( gattChar );
                    }
                }
            }
        }

        @Override
        public void onCharacteristicRead( BluetoothGatt gatt,
                                          BluetoothGattCharacteristic characteristic,
                                          int status ) {
            if ( status == BluetoothGatt.GATT_SUCCESS ) {
                if ( characteristic.getValue() != null ) {

                    StringBuilder sb = new StringBuilder();
                    for ( byte b : characteristic.getValue() ) {
                        sb.append( ( char ) b );
                    }

                    BluetoothGATTMessenger.send( "\t\tValue Found: " + sb.toString() +
                            " -> " + characteristicDecoder( characteristic.getUuid() ) );
                }
            }
        }

        @Override
        public void onCharacteristicChanged( BluetoothGatt gatt,
                                             BluetoothGattCharacteristic characteristic ) {
            BluetoothGATTMessenger.send( "\t\tChar Changed: " + characteristic );
        }

        @Override
        public void onCharacteristicWrite( BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status ) {
            super.onCharacteristicWrite( gatt, characteristic, status );
            BluetoothGATTMessenger.send( "\t\tChar Write: " + characteristic );
        }

        @Override
        public void onDescriptorRead( BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status ) {
            super.onDescriptorRead( gatt, descriptor, status );
            StringBuilder sb = new StringBuilder();
            for ( byte b : descriptor.getValue() ) {
                sb.append( ( char ) b );
            }
            BluetoothGATTMessenger.send( "\t\tDesc Read: " + sb.toString() );
        }
    };

    private String getMajorBits( UUID uuid ) {
        return String.format("%8x", uuid.getMostSignificantBits() >> 32 ).trim();
    }

    private String serviceDecoder( UUID uuid ) {
        String num = getMajorBits( uuid );
        switch( num ) {
            case "1800":
                return "Generic Access";
            case "1801":
                return "Generic Attribute";
            case "180a":
                return "Device Information";
            case "6e400001":
                return "Firmware Specified Service UUID";
            default: return num;
        }
    }

    private String characteristicDecoder( UUID uuid ) {
        String num = getMajorBits( uuid );
        switch ( num ) {
            case "2a00":
                return "Device Name";
            case "2a01":
                return "Appearance";
            case "2a04":
                return "Peripheral Preferred Connection Parameters";
            case "2a05":
                return "Service Changed";
            case "2a24":
                return "Model Number String";
            case "2a26":
                return "Firmware Revision String";
            case "2a27":
                return "Hardware Revision String";
            case "2a28":
                return "Software Revision String";
            case "2a29":
                return "Manufacturer Name String";
            case "2aa6":
                return "Central Address Resolution";
            case "6e400002":
                return "Receive Transmission Characteristic";
            case "6e400003":
                return "Transmit Information Characteristic";
            default: return num;
        }
    }
}
