package com.sb2030.loggerapp.resources;

import android.content.SharedPreferences;

import com.sb2030.loggerapp.data.types.LogMessage;

/**
 * Settings
 * A class that can be statically referenced anywhere in the app, and can be updated by any
 * class, well, except for the final values.
 */
public class Settings {
	// Just debug variables.
	public static boolean DEBUG_ON = true;
	public static boolean SUPER_DEBUG_ON = false;

	// Do we want to do demos for class or whatnot?
	// All this does it make it so three ships appear at a time
	// and show some data.
	public static boolean DEMO_ON = false;
	public static boolean DO_AWS = false;

	// This is used to apply layouts.
	public static boolean IS_PORTRAIT = true;

	// For BT related items
	public static boolean IS_BLUETOOTH_ON = false;
	public static boolean IS_BLUETOOTH_CONNECTED = false;
	public static boolean IS_IN_VM = false;
	public static boolean FILTER_BTLE_SEARCH_RESULTS = false;
	public static boolean ONLY_ADD_UNIQUE_DEVICES_TO_SHIP_LIST = true;
	public static boolean SHOULD_SHOW_BT_TRANSACTIONS = true;

	// User customizable settings
	public static int     BTLE_SEARCH_TIMEOUT = 10000;
	public static int	  LOG_ENTRIES_TO_SHOW = 100;

	// Class Subscribed Switches
	public static boolean HAS_MAIN_SET_UP = false;

	// AWS things
	public static boolean AWS_ENABLED = false;

	// This function syncs the settings we use to the SharedPreferences that are modified in the
    // settings pane.
	public static void sync( SharedPreferences sp ) {
		Settings.SHOULD_SHOW_BT_TRANSACTIONS = sp.getBoolean( "show_bt_transactions", true );
		Settings.BTLE_SEARCH_TIMEOUT = Integer.parseInt( sp.getString( "scan_timeout_seconds", "10000" ) );
		String val = sp.getString( "debug_level", "0" );
		switch ( val ) {
			case "0":
				Settings.DEBUG_ON = false;
				Settings.SUPER_DEBUG_ON = false;
				break;
			case "1":
				Settings.DEBUG_ON = true;
				Settings.SUPER_DEBUG_ON = false;
				break;
			case "2":
				Settings.DEBUG_ON = true;
				Settings.SUPER_DEBUG_ON = true;
				break;
		}
		Settings.DEMO_ON = sp.getBoolean( "demo_mode_on", false );
		Settings.DO_AWS = sp.getBoolean( "aws_enable", true );
		Settings.LOG_ENTRIES_TO_SHOW = Integer.parseInt( sp.getString( "log_entries_to_show", "10000" ) ); // All entries
		if ( DEBUG_ON )
			Log.log( "Settings", LogMessage.MESSAGE_TYPE.DEBUG,
				"Synced Settings.\n" +
						"SHOULD_SHOW_BT_TRANSACTIONS: " + SHOULD_SHOW_BT_TRANSACTIONS + "\n" +
						"BTLE_SEARCH_TIMEOUT: " + BTLE_SEARCH_TIMEOUT + "\n" +
						"DEBUG_ON: " + DEBUG_ON + "\n" +
						"SUPER_DEBUG_ON: " + SUPER_DEBUG_ON + "\n" +
						"DEMO_ON: " + DEMO_ON + "\n" +
						"DO_AWS: " + DO_AWS + "\n" +
						"LOG_ENTRIES_TO_SHOW: " + LOG_ENTRIES_TO_SHOW );
	}

}
