package com.sb2030.loggerapp.resources;

import com.sb2030.loggerapp.data.types.LogMessage;
import com.sb2030.loggerapp.messaging.log.LogMessenger;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class Log {

	private static final ArrayList< LogMessage > allLogs = new ArrayList<>();
	private static final ArrayList< LogMessage > newLogs = new ArrayList<>();

	/**
	 * Log
	 * Logs a message in an organized way
	 * Includes the date and time if Settings.SUPER_DEBUG_ON is set to true
	 * @param msg The message to print
	 */
	public static void log( LogMessage msg ) {
		if ( Settings.DEBUG_ON ) {
			System.out.println( "[ " + msg.getType() + getDateTime() + " ] " + msg.getMessage() );
		}
		allLogs.add( msg );
		newLogs.add( msg );

		LogMessenger.update( getNewLogs() );
	}

	/**
	 * Log
	 * Logs a message but allows a user to really specify everything
	 * @param source The class that the log message is coming from. This is just getClass.getSimpleName();
	 * @param msgType The type of message so it can be displayed with the proper icon
	 * @param msg The actual message the log is portraying.
	 */
	public static void log ( String source, LogMessage.MESSAGE_TYPE msgType, String msg ) {
		log( new LogMessage( source, msgType, msg ) );
	}

	public static void log( String msg ) {
		log( new LogMessage( "Unspecified", LogMessage.MESSAGE_TYPE.INFORMATION, msg ) );
	}

	/**
	 * GetDateTime
	 * A better looking way to get the date of the current print, if Settings.SUPER_DEBUG_ON is true
	 *
	 * @return The date and time as a string
	 */
	private static String getDateTime() {
		// If we should print, format the date output, and then return the formatted Date
		if ( Settings.SUPER_DEBUG_ON )
			return " - " + new SimpleDateFormat( "MM/dd/yyyy - HH:mm", Locale.ENGLISH ).format( new Date() );
		return "";
	}

	/**
	 * GetAllLogs
	 * Returns all of the logs. This is used when the LogActivity is coming back from the dead.
	 * @return All of the logs
	 */
	public static ArrayList< LogMessage > getAllLogs() {
		return allLogs;
	}

	/**
	 * Get new logs. Basically just makes a copy of itself, clears itself, and then returns whatever it had.
	 * @return All of the new logs
	 */
	public static ArrayList< LogMessage > getNewLogs() {
		ArrayList< LogMessage > newL = new ArrayList<>( newLogs );
		newLogs.clear();
		return newL;
	}
}
