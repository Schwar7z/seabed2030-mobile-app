package com.sb2030.loggerapp.sample;

import com.sb2030.loggerapp.data.types.Ship;

import java.util.Random;

public class SampleBoatData {

	private static int start = 0;

	public static Ship[] getSampleConnection( int amount ) {

		Ship[] temp = new Ship[ amount ];
		Random r = new Random( System.currentTimeMillis() );
		
		for ( int x = 0; x < amount; x += 1 ){
			String id = String.valueOf( Math.abs( r.nextLong() ) );
			String name = "Ship " + ( x + start );

			temp[ x ] = new Ship( id, name );
		}
		start += amount;
		return temp;
	}
}
